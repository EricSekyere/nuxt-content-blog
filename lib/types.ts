export interface Article {
  author: string
  title: string
  body: Object
  excerpt: Object
  dir: string
  extension: string
  path: string
  slug: string
  toc: string
  createdAt: string
  updatedAt: object
}
