// Force import of ambient type declarations

declare module '*.vue' {
  import Vue from 'vue'
  export default Vue
}
